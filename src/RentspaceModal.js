import React, {Component} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input
} from "reactstrap";
import ValidationError from "./ValidationError";

export default class RentspaceModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      rentspaceAddress: this.props.rentspace.address,
      rentspaceLogo: this.props.rentspace.logo,
      errorText: null
    };

    this.toggle = this.toggle.bind(this);
    this.handleRentspaceAddressChange = this.handleRentspaceAddressChange.bind(
      this
    );
    this.handleRentspaceLogoChange = this.handleRentspaceLogoChange.bind(this);
    this.save = this.save.bind(this);
    this.validate = this.validate.bind(this);
  }

  toggle() {
    if (this.state.modal) {
      this.setState({
        modal: false,
        rentspaceAddress: null,
        rentspaceLogo: null,
        errorText: null
      });
    } else {
      this.setState({
        modal: true,
        rentspaceAddress: this.props.rentspace.address,
        rentspaceLogo: this.props.rentspace.logo,
        errorText: null
      });
    }
  }

  handleRentspaceAddressChange(event) {
    this.setState({rentspaceAddress: event.target.value});
  }

  handleRentspaceLogoChange(event) {
    this.setState({rentspaceLogo: event.target.value});
  }

  save() {
    if (!this.validate()) {
      return;
    }

    const rentspace = {
      id: this.props.rentspace.id,
      address: this.state.rentspaceAddress,
      logo: this.state.rentspaceLogo
    };
    const requestUrl = "http://localhost:8080/rentspace";
    const method = this.props.rentspace.id > 0 ? "PUT" : "POST";

    fetch(requestUrl, {
      method: method,
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(rentspace)
    }).then(response => {
      this.props.onRentspaceChange();
      this.toggle();
    });
  }

  validate() {
    if (
      this.state.rentspaceAddress == null ||
      this.state.rentspaceAddress.length < 1
    ) {
      this.setState({errorText: "Rentspace address missing"});
      return false;
    }
    if (
      this.state.rentspaceLogo == null ||
      this.state.rentspaceLogo.length < 1
    ) {
      this.setState({errorText: "Rentspace picture missing"});
      return false;
    }

    this.setState({errorText: null});
    return true;
  }

  render() {
    return (
      <div className="rentspaceRowButtons">
        <Button color={this.props.color} onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>
            {this.props.modalTitle}
          </ModalHeader>
          <ModalBody>
            <ValidationError errorText={this.state.errorText} />
            <Form>
              <FormGroup>
                <Input
                  placeholder="Rentspace Address"
                  value={this.state.rentspaceAddress}
                  onChange={this.handleRentspaceAddressChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Rentspace Picture"
                  value={this.state.rentspaceLogo}
                  onChange={this.handleRentspaceLogoChange}
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.save}>
              Save
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
