import React, {Component} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input
} from "reactstrap";
import ValidationError from "./ValidationError";

export default class TenantModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      tenantName: this.props.tenant.name,
      tenantEmail: this.props.tenant.email,
      tenantPhone: this.props.tenant.phone,
      tenantPersonalId: this.props.tenant.personal_id,
      tenantAddress: this.props.tenant.address,
      errorText: null
    };

    this.toggle = this.toggle.bind(this);
    this.handleTenantNameChange = this.handleTenantNameChange.bind(this);
    this.handleTenantEmailChange = this.handleTenantEmailChange.bind(this);
    this.handleTenantPhoneChange = this.handleTenantPhoneChange.bind(this);
    this.handleTenantPersonalIdChange = this.handleTenantPersonalIdChange.bind(
      this
    );
    this.handleTenantAddressChange = this.handleTenantAddressChange.bind(this);
    this.save = this.save.bind(this);
    this.validate = this.validate.bind(this);
  }

  toggle() {
    if (this.state.modal) {
      this.setState({
        modal: false,
        tenantName: null,
        tenantEmail: null,
        tenantPhone: null,
        tenantPersonalId: null,
        tenantAddress: null,
        errorText: null
      });
    } else {
      this.setState({
        modal: true,
        tenantName: this.props.tenant.name,
        tenantEmail: this.props.tenant.email,
        tenantPhone: this.props.tenant.phone,
        tenantPersonalId: this.props.tenant.personal_id,
        tenantAddress: this.props.tenant.address,
        errorText: null
      });
    }
  }

  handleTenantNameChange(event) {
    this.setState({tenantName: event.target.value});
  }

  handleTenantEmailChange(event) {
    this.setState({tenantEmail: event.target.value});
  }

  handleTenantPhoneChange(event) {
    this.setState({tenantPhone: event.target.value});
  }

  handleTenantPersonalIdChange(event) {
    this.setState({tenantPersonalId: event.target.value});
  }

  handleTenantAddressChange(event) {
    this.setState({tenantAddress: event.target.value});
  }

  save() {
    if (!this.validate()) {
      return;
    }

    const tenant = {
      id: this.props.tenant.id,
      name: this.props.tenant.name,
      email: this.props.tenant.email,
      phone: this.props.tenant.phone,
      personal_id: this.state.personal_id,
      address: this.state.tenantAddress
    };
    const requestUrl = "http://localhost:8080/tenant";
    const method = this.props.tenant.id > 0 ? "PUT" : "POST";

    fetch(requestUrl, {
      method: method,
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(tenant)
    }).then(response => {
      this.props.onTenantChange();
      this.toggle();
    });
  }

  validate() {
    if (this.state.tenantName == null || this.state.tenantName.length < 1) {
      this.setState({errorText: "Tenant name missing"});
      return false;
    }
    if (this.state.tenantEmail == null || this.state.tenantEmail.length < 1) {
      this.setState({errorText: "Tenant email missing"});
      return false;
    }
    if (this.state.tenantPhone == null || this.state.tenantPhone.length < 1) {
      this.setState({errorText: "Tenant phone missing"});
      return false;
    }
    if (
      this.state.tenantPersonalId == null ||
      this.state.tenantPersonalId.length < 1
    ) {
      this.setState({errorText: "Tenant personal ID missing"});
      return false;
    }
    if (
      this.state.tenantAddress == null ||
      this.state.tenantAddress.length < 1
    ) {
      this.setState({errorText: "Tenant address missing"});
      return false;
    }

    this.setState({errorText: null});
    return true;
  }

  render() {
    return (
      <div className="tenantRowButtons">
        <Button color={this.props.color} onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>
            {this.props.modalTitle}
          </ModalHeader>
          <ModalBody>
            <ValidationError errorText={this.state.errorText} />
            <Form>
              <FormGroup>
                <Input
                  placeholder="Tenant Name"
                  value={this.state.tenantName}
                  onChange={this.handleTenantNameChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Tenant Email"
                  value={this.state.tenantEmail}
                  onChange={this.handleTenantEmailChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Tenant Phone"
                  value={this.state.tenantPhone}
                  onChange={this.handleTenantPhoneChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Tenant Personal ID"
                  value={this.state.tenantPersonalId}
                  onChange={this.handleTenantPersonalIdChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Tenant Address"
                  value={this.state.tenantAddress}
                  onChange={this.handleTenantAddressChange}
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.save}>
              Save
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
