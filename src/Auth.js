function login(username, password) {
  return fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({username: username, password: password})
  }).then(response => this.setToken(response));
}

function setToken(response) {
  if (response != null && response.headers.get("Authorization") != null) {
    localStorage.setItem("token", response.headers.get("Authorization"));
  }
}

function clearToken() {
  localStorage.removeItem("token");
}

function getToken() {
  return localStorage.getItem("token");
}

function addAuthorizationHeader(headers) {
  headers["Authorization"] = this.getToken();
  return headers;
}

function isAuthenticated() {
  return this.getToken() != null;
}

function textToJson(text, defaultValue) {
  return text.length ? JSON.parse(text) : defaultValue;
}

function clearTokenIfUnauthorized(response) {
  if (response.status === 403) {
    this.clearToken();
    return Promise.reject("Unauthorized request.");
  } else {
    return Promise.resolve(response);
  }
}

export default {
  login,
  setToken,
  clearToken,
  getToken,
  addAuthorizationHeader,
  isAuthenticated,
  textToJson,
  clearTokenIfUnauthorized
};
