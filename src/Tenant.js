import React, {Component} from "react";
import {Button} from "reactstrap";
import TenantModal from "./TenantModal";
import Auth from "./Auth.js";

export default class Tenant extends Component {
  constructor(props) {
    super(props);

    this.deleteTenant = this.deleteTenant.bind(this);
  }

  deleteTenant(id) {
    if (window.confirm("Are you sure you want to delete this Tenant?")) {
      fetch("http://localhost:8080/tenant/" + id, {
        method: "DELETE",
        headers: Auth.addAuthorizationHeader({})
      }).then(response => this.props.onTenantChange());
    }
  }

  render() {
    return (
      <tr>
        <td>{this.props.tenant.id}</td>
        <td>{this.props.tenant.name}</td>
        <td>{this.props.tenant.email}</td>
        <td>{this.props.tenant.phone}</td>
        <td>{this.props.tenant.personalId}</td>
        <td>{this.props.tenant.address}</td>
        <td>
          <div className="tenantRowButtons">
            <Button
              className="btn btn-danger"
              onClick={() => this.deleteTenant(this.props.tenant.id)}
            >
              Delete
            </Button>
          </div>
          <TenantModal
            buttonLabel="Change"
            modalTitle="Change Tenant"
            tenant={this.props.tenant}
            onTenantChange={this.props.onTenantChange}
            color="dark"
          />
        </td>
      </tr>
    );
  }
}
