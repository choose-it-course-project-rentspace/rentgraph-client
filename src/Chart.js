import React, {Component} from "react";
import ReactDOM from "react-dom";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import AutoSizer from "react-virtualized/dist/commonjs/AutoSizer";
import Dimensions from "react-dimensions";
import Auth from "./Auth.js";
import "./App.js";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

class Chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chart: {},
      currentVal: "none",
      colors: null
    };
    this.getColors = this.getColors.bind(this);

    this.renderComplete = this.renderComplete.bind(this);
    this.sliceClicked = this.sliceClicked.bind(this);
  }

  componentDidMount() {
    this.getColors();
  }

  getColors() {
    fetch("http://localhost:8080/colors", {
      headers: Auth.addAuthorizationHeader({})
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => response.json())
      .then(colors =>
        this.setState({colors: colors}, this.processColors(colors))
      )
      .catch(error => console.log("Unauthenticated"));
  }

  processColors = colors => {
    const chartConfigs = {
      type: "Pie2D",
      width: "100%",
      height: "300%",
      dataFormat: "json",
      dataSource: {
        chart: {
          caption: "Contract Health",
          plottooltext:
            "<b>$percentValue</b> of contracts (<b>$value contracts</b>) are $label, click and scroll down for details!",
          showPercentValues: "1",
          useDataPlotColorForLabels: "1",
          enableMultiSlicing: "0",
          theme: "fusion",
          bgImage:
            "https://images.pexels.com/photos/1587947/pexels-photo-1587947.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
          bgImageAlpha: "10",
          bgImageScale: "100%"
        },
        data: [
          {
            label: "All right!",
            value: colors.green,
            labelDisplay: "in more than 90 days"
          },
          {
            label: "Expiring soon",
            value: colors.yellow,
            palettecolors: "5d62b5,29c3be,f2726f",
            labelDisplay: "in less than 90 days"
          },
          {
            label: "Expired",
            value: colors.red
          },
          {
            label: "Missing contract info",
            value: colors.white
          }
        ]
      }
    };
    this.setState({chartConfigs: chartConfigs});
  };

  renderComplete(chart) {
    this.setState({chart});
  }

  // Event callback for 'dataplotClick'.
  // Makes the relevant radio active when a plot is clicked.

  sliceClicked(eventObj, dataObj) {
    //console.log(dataObj);
    //console.log(eventObj);

    const color = !eventObj.data.isSliced
      ? this.retrieveColor(dataObj.index)
      : 0;

    this.setState(
      {
        currentVal: eventObj.data.isSliced ? "none" : eventObj.data.dataIndex
      },
      () => {
        this.props.onRentalChange(color);
      }
    );
  }

  retrieveColor = index => index + 1;

  render() {
    return (
      <div>
        <ReactFC
          {...this.state.chartConfigs}
          onRender={this.renderComplete}
          fcEvent-dataplotClick={this.sliceClicked}
        />
        <br />
        <br />
      </div>
    );
  }
}
export default Chart;
