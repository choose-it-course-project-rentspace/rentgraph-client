import React, {Component} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input
} from "reactstrap";
import Auth from "./Auth.js";
import ValidationError from "./ValidationError";

export default class RentalModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      rentalAddress: this.props.rental.address,
      rentalTenants: this.props.rental.tenants,
      rentalEndDate: this.props.rental.endDate,
      authenticated: false
    };

    this.toggle = this.toggle.bind(this);
    this.handleRentalAddressChange = this.handleRentalAddressChange.bind(this);
    this.handleRentalTenantsChange = this.handleRentalTenantsChange.bind(this);
    this.handleRentalEndDateChange = this.handleRentalEndDateChange.bind(this);
    this.save = this.save.bind(this);
    this.validate = this.validate.bind(this);
  }

  toggle() {
    if (this.state.modal) {
      this.setState({
        modal: false,
        rentalAddress: null,
        rentalTenants: null,
        rentalEndDate: null,
        errorText: null
      });
    } else {
      this.setState({
        modal: true,
        rentalAddress: this.props.rental.address,
        rentalTenants: this.props.rental.tenants,
        rentalEndDate: this.props.rental.endDate,
        errorText: null
      });
    }
  }

  handleRentalAddressChange(event) {
    this.setState({rentalAddress: event.target.value});
  }

  handleRentalTenantsChange(event) {
    this.setState({rentalTenants: event.target.value});
  }

  handleRentalEndDateChange(event) {
    this.setState({rentalEndDate: event.target.value});
  }

  save() {
    if (!this.validate()) {
      return;
    }

    const rental = {
      address: this.state.rentalAddress,
      tenants: this.state.rentalTenants,
      endDate: this.state.rentalEndDate
    };

    const requestUrl = "http://localhost:8080/relevant";
    const method = "POST";
    //    const method = this.props.rental.endDate > 0 ? "PUT" : "POST";

    fetch(requestUrl, {
      method: method,
      headers: Auth.addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify(rental)
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => {
        this.props.onRentalChange();
        this.toggle();
      });
  }

  validate() {
    if (
      this.state.rentalAddress == null ||
      this.state.rentalAddress.length < 1
    ) {
      this.setState({errorText: "Rental address missing"});
      return false;
    }

    if (
      this.state.rentalEndDate == null ||
      this.state.rentalEndDate.length < 1
    ) {
      this.setState({errorText: "Rental end date missing"});
      return false;
    }

    //    if (this.state.rentalLogo == null || this.state.rentalLogo.length < 1) {
    //      this.setState({errorText: "Rental picture missing"});
    //      return false;
    //    }

    this.setState({errorText: null});
    return true;
  }

  render() {
    if (Auth.isAuthenticated()) {
      return (
        <div className="rentalRowButtons">
          <Button color={this.props.color} onClick={this.toggle}>
            {this.props.buttonLabel}
          </Button>
          <Modal isOpen={this.state.modal} toggle={this.toggle}>
            <ModalHeader toggle={this.toggle}>
              {this.props.modalTitle}
            </ModalHeader>
            <ModalBody>
              <ValidationError errorText={this.state.errorText} />
              <Form>
                <FormGroup>
                  <Input
                    placeholder="Address"
                    value={this.state.rentalAddress}
                    onChange={this.handleRentalAddressChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    placeholder="Tenants"
                    value={this.state.rentalTenants}
                    onChange={this.handleRentalTenantsChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    placeholder="End Date"
                    value={this.state.rentalEndDate}
                    onChange={this.handleRentalEndDateChange}
                  />
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.save}>
                Save
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    }
  }
}
