import React, {Component} from "react";
import {Button} from "reactstrap";
import RentalModal from "./RentalModal";
import Auth from "./Auth.js";

export default class Rental extends Component {
  constructor(props) {
    super(props);

    this.deleteRental = this.deleteRental.bind(this);
  }

  deleteRental(id) {
    if (window.confirm("Are you sure you want to delete this Rentspace?")) {
      fetch("http://localhost:8080/rental/" + id, {
        method: "DELETE",
        headers: Auth.addAuthorizationHeader({})
      }).then(response => this.props.onRentalChange());
    }
  }

  render() {
    return (
      <tr>
        <td>{this.props.sequenceNumber + 1}</td>
        <td>{this.props.rental.address}</td>
        <td>{this.props.rental.tenants}</td>
        <td>{this.props.rental.endDate}</td>
      </tr>
    );
  }
}

//          <div className="rentalRowButtons">
//            <Button
//              className="btn btn-danger"
//              onClick={() => this.deleteRental(this.props.rental.id)}
//            >
//              Delete
//            </Button>
//          </div>
//          <RentalModal
//            buttonLabel="Change"
//            modalTitle="Change Rentspace"
//            rental={this.props.rental}
//            onRentalChange={this.props.onRentalChange}
//            color="dark"
//          />
