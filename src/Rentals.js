import React, {Component} from "react";
import {Table} from "reactstrap";
import Rental from "./Rental";

export default class Rentals extends Component {
  render() {
    return (
      <Table>
        <thead className="thead-dark">
          <tr>
            <th>ID</th>
            <th>Address</th>
            <th>Tenant</th>
            <th>Ends at</th>
          </tr>
        </thead>
        <tbody>
          {this.props.rentals.map((rental, index) => (
            <Rental
              key={index} //varem oli rental.id... miks?
              rental={rental}
              sequenceNumber={index}
              onRentalChange={this.props.onRentalChange}
            />
          ))}
        </tbody>
      </Table>
    );
  }
}
