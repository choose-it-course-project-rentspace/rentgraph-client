import React, {Component} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input
} from "reactstrap";
import ValidationError from "./ValidationError";

export default class ContractModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      contractStartDate: this.props.contract.start_date,
      contractEndDate: this.props.contract.end_date,
      contractTenantId: this.props.contract.tenant_id,
      contractRentalId: this.props.contract.rental_id,
      contractActive: this.props.contract.active,
      errorText: null
    };

    this.toggle = this.toggle.bind(this);
    this.handleContractStartDateChange = this.handleContractStartDateChange.bind(
      this
    );
    this.handleContractEndDateChange = this.handleContractEndDateChange.bind(
      this
    );
    this.handleContractTenantIdChange = this.handleContractTenantIdChange.bind(
      this
    );
    this.handleContractRentalIdChange = this.handleContractRentalIdChange.bind(
      this
    );
    this.handleContractActiveChange = this.handleContractActiveChange.bind(
      this
    );
    this.save = this.save.bind(this);
    this.validate = this.validate.bind(this);
  }

  toggle() {
    if (this.state.modal) {
      this.setState({
        modal: false,
        contractStartDate: null,
        contractEndDate: null,
        contractTenantId: null,
        contractRentalId: null,
        contractActive: null,
        errorText: null
      });
    } else {
      this.setState({
        modal: true,
        contractStartDate: this.props.contract.start_date,
        contractEndDate: this.props.contract.end_date,
        contractTenantId: this.props.contract.tenant_id,
        contractRentalId: this.props.contract.rental_id,
        contractActive: this.props.contract.active,
        errorText: null
      });
    }
  }

  handleContractStartDateChange(event) {
    this.setState({contractStartDate: event.target.value});
  }

  handleContractEndDateChange(event) {
    this.setState({contractEndDate: event.target.value});
  }

  handleContractTenantIdChange(event) {
    this.setState({contractTenantId: event.target.value});
  }

  handleContractRentalIdChange(event) {
    this.setState({contractRentalId: event.target.value});
  }

  handleContractActiveChange(event) {
    this.setState({contractActive: event.target.value});
  }

  save() {
    if (!this.validate()) {
      return;
    }

    const contract = {
      id: this.props.contract.id,
      start_date: this.props.contractStartDate,
      end_date: this.props.contractEndDate,
      tenant_id: this.props.contractTenantId,
      rental_id: this.state.contractRentalId,
      active: this.state.contractActive
    };
    const requestUrl = "http://localhost:8080/contract";
    const method = this.props.contract.id > 0 ? "PUT" : "POST";

    fetch(requestUrl, {
      method: method,
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(contract)
    }).then(response => {
      this.props.onContractChange();
      this.toggle();
    });
  }

  validate() {
    if (
      this.state.contractStartDate == null ||
      this.state.contractStartDate.length < 1
    ) {
      this.setState({errorText: "Contract start date missing"});
      return false;
    }
    if (
      this.state.contractEndDate == null ||
      this.state.contractEndDate.length < 1
    ) {
      this.setState({errorText: "Contract end date missing"});
      return false;
    }
    if (
      this.state.contractTenantId == null ||
      this.state.contractTenantId.length < 1
    ) {
      this.setState({errorText: "Contract tenant ID missing"});
      return false;
    }
    if (
      this.state.contractRentalId == null ||
      this.state.contractRentalId.length < 1
    ) {
      this.setState({errorText: "Contract rental ID missing"});
      return false;
    }
    if (
      this.state.contractActive == null ||
      this.state.contractActive.length < 1
    ) {
      this.setState({errorText: "Contract active status (1 or 0) missing"});
      return false;
    }

    this.setState({errorText: null});
    return true;
  }

  render() {
    return (
      <div className="contractRowButtons">
        <Button color={this.props.color} onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>
            {this.props.modalTitle}
          </ModalHeader>
          <ModalBody>
            <ValidationError errorText={this.state.errorText} />
            <Form>
              <FormGroup>
                <Input
                  placeholder="Start Date"
                  value={this.state.contractStartDate}
                  onChange={this.handleContractStartDateChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="End Date"
                  value={this.state.contractEndDate}
                  onChange={this.handleContractEndDateChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Tenant ID"
                  value={this.state.contractTenantId}
                  onChange={this.handleContractTenantIdChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Rental ID"
                  value={this.state.contractRentalId}
                  onChange={this.handleContractRentalIdChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Active?"
                  value={this.state.contractActive}
                  onChange={this.handleContractActiveChange}
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.save}>
              Save
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
