import React, {Component} from "react";
import "./App.css";
import Rentals from "./Rentals";
import Rentspaces from "./Rentspaces";
import Contracts from "./Contracts";
import Tenants from "./Tenants";
import RentalModal from "./RentalModal";
import RentspaceModal from "./RentspaceModal";
import ContractModal from "./ContractModal";
import TenantModal from "./TenantModal";
import Auth from "./Auth.js";
import Login from "./Login.js";
import Chart from "./Chart.js";
import {Button} from "reactstrap";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rentals: [],
      rentspaces: [],
      contracts: [],
      tenants: [],
      authenticated: false,
      tablesvisible: false
    };

    this.getRentals = this.getRentals.bind(this);
    this.getRentspaces = this.getRentspaces.bind(this);
    this.getContracts = this.getContracts.bind(this);
    this.getTenants = this.getTenants.bind(this);
  }

  componentDidMount() {
    this.getRentals();
    this.getRentspaces();
    this.getContracts();
    this.getTenants();
  }

  getRentals(color) {
    if (color == null) {
      color = 0;
    }

    fetch("http://localhost:8080/rentals/" + color, {
      headers: Auth.addAuthorizationHeader({})
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => response.json())
      .then(rentals => this.setState({rentals: rentals}))
      .catch(error => this.setState({rentals: []}));
  }

  getRentspaces() {
    fetch("http://localhost:8080/rentspaces", {
      headers: Auth.addAuthorizationHeader({})
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => response.json())
      .then(rentspaces => this.setState({rentspaces: rentspaces}))
      .catch(error => this.setState({rentspaces: []}));
  }

  getContracts() {
    fetch("http://localhost:8080/contracts", {
      headers: Auth.addAuthorizationHeader({})
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => response.json())
      .then(contracts => this.setState({contracts: contracts}))
      .catch(error => this.setState({contracts: []}));
  }

  getTenants() {
    fetch("http://localhost:8080/tenants", {
      headers: Auth.addAuthorizationHeader({})
    })
      .then(response => Auth.clearTokenIfUnauthorized(response))
      .then(response => response.json())
      .then(tenants => this.setState({tenants: tenants}))
      .catch(error => this.setState({tenants: []}));
  }

  render() {
    if (Auth.isAuthenticated()) {
      return (
        <div id="mainContainer">
          <div class="inImage" />

          <div class="content">
            <Button
              color="primary"
              size="lg"
              onClick={() => {
                Auth.clearToken();
                this.setState({rentals: []});
              }}
            >
              Log out
            </Button>
            <br />
            <Chart
              rentals={this.state.rentals}
              onRentalChange={this.getRentals}
            />
            <Button
              color="primary"
              size="lg"
              onClick={() => {
                this.setState({tablesvisible: !this.state.tablesvisible});
              }}
            >
              {this.state.tablesvisible
                ? "Hide Data Tables"
                : "Show All Tables"}
            </Button>
            <br />
            <Rentals
              rentals={this.state.rentals}
              onRentalChange={this.getRentals}
            />
            <br />
            <br />
            <RentalModal
              buttonLabel="Quick-Add Rentspace"
              modalTitle="Add a Rentspace"
              rental={{
                id: 0,
                address: null,
                logo: null
              }}
              onRentalChange={this.getRentals}
              color="primary"
            />
            <br />
            <br />
            <div style={{display: this.state.tablesvisible ? "block" : "none"}}>
              <h1> Rental Objects </h1>
              <Rentspaces
                rentspaces={this.state.rentspaces}
                onRentspaceChange={this.getRentspaces}
              />
              <br />
              <br />
              <RentspaceModal
                buttonLabel="Add Rentspace"
                modalTitle="Add a Rentspace"
                rentspace={{
                  id: 0,
                  address: null
                }}
                onRentspaceChange={this.getRentspaces}
                color="primary"
              />
              <br />
              <br />
              <h1> Contracts </h1>
              <Contracts
                contracts={this.state.contracts}
                onContractChange={this.getContracts}
              />
              <br />
              <br />
              <ContractModal
                buttonLabel="Add Contract"
                modalTitle="Add a Contract"
                contract={{
                  id: 0,
                  start_date: null,
                  end_date: null,
                  tenant_id: null,
                  rental_id: null,
                  active: null
                }}
                onContractChange={this.getContracts}
                color="primary"
              />
              <br />
              <br />
              <h1> Tenants </h1>
              <Tenants
                tenants={this.state.tenants}
                onTenantChange={this.getTenants}
              />
              <br />
              <br />
              <TenantModal
                buttonLabel="Add Tenant"
                modalTitle="Add a Tenant"
                tenant={{
                  id: 0,
                  name: null,
                  email: null,
                  phone: null,
                  personal_id: null,
                  address: null
                }}
                onTenantChange={this.getTenants}
                color="primary"
              />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <Login
            postSuccessAction={
              (this.getRentals,
              this.getRentspaces,
              this.getContracts,
              this.getTenants)
            }
          />
        </div>
      );
    }
  }
}
