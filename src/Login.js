import React, {Component} from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input
} from "reactstrap";
import "./Login.css";
import Auth from "./Auth.js";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null
    };

    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlePassWordChange = this.handlePassWordChange.bind(this);
  }



  handleUserNameChange(event) {
    this.setState({username: event.target.value});
  }

  handlePassWordChange(event) {
    this.setState({password: event.target.value});
  }

  login = () => {
    Auth.login(this.state.username, this.state.password).then(response => {
      this.props.postSuccessAction();
    });
  };

  render() {
    return (

<div class="loginContainer">

    <div class="inImage"></div>

      <div class="content">

        <h1>Welcome to RentGraph!</h1>

        <Form>
          <FormGroup>
            <Input
              placeholder="Username"
              value={this.state.username}
              onChange={this.handleUserNameChange}
            />
          </FormGroup>
          <FormGroup>
            <Input
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handlePassWordChange}
            />
          </FormGroup>
        </Form>

        <div class="button-margin">
        <Button onClick={this.login}>Log in</Button>
        </div>

      </div>
</div>

    );
  }
}

export default Login;
