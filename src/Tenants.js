import React, {Component} from "react";
import {Table} from "reactstrap";
import Tenant from "./Tenant";

export default class Tenants extends Component {
  render() {
    return (
      <Table>
        <thead className="thead-dark">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Personal ID</th>
            <th>Address</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          {this.props.tenants.map(tenant => (
            <Tenant
              key={tenant.id}
              tenant={tenant}
              onTenantChange={this.props.onTenantChange}
            />
          ))}
        </tbody>
      </Table>
    );
  }
}
