import React, {Component} from "react";
import {Table} from "reactstrap";
import Contract from "./Contract";

export default class Contracts extends Component {
  render() {
    return (
      <Table>
        <thead className="thead-dark">
          <tr>
            <th>ID</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Tenant ID</th>
            <th>Rental ID</th>
            <th>Active?</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          {this.props.contracts.map(contract => (
            <Contract
              key={contract.id}
              contract={contract}
              onContractChange={this.props.onContractChange}
            />
          ))}
        </tbody>
      </Table>
    );
  }
}
