import React, {Component} from "react";
import {Button} from "reactstrap";
import RentspaceModal from "./RentspaceModal";
import Auth from "./Auth.js";

export default class Rentspace extends Component {
  constructor(props) {
    super(props);

    this.deleteRentspace = this.deleteRentspace.bind(this);
  }

  deleteRentspace(id) {
    if (window.confirm("Are you sure you want to delete this Rentspace?")) {
      fetch("http://localhost:8080/rentspace/" + id, {
        method: "DELETE",
        headers: Auth.addAuthorizationHeader({})
      }).then(response => this.props.onRentspaceChange());
    }
  }

  render() {
    return (
      <tr>
        <td>{this.props.rentspace.id}</td>
        <td>{this.props.rentspace.address}</td>
        <td>
          <img src={this.props.rentspace.logo} height="50" alt="Logo" />
        </td>
        <td>
          <div className="rentspaceRowButtons">
            <Button
              className="btn btn-danger"
              onClick={() => this.deleteRentspace(this.props.rentspace.id)}
            >
              Delete
            </Button>
          </div>
          <RentspaceModal
            buttonLabel="Change"
            modalTitle="Change Rentspace"
            rentspace={this.props.rentspace}
            onRentspaceChange={this.props.onRentspaceChange}
            color="dark"
          />
        </td>
      </tr>
    );
  }
}
