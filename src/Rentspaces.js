import React, {Component} from "react";
import {Table} from "reactstrap";
import Rentspace from "./Rentspace";

export default class Rentspaces extends Component {
  render() {
    return (
      <Table>
        <thead className="thead-dark">
          <tr>
            <th>ID</th>
            <th>Address</th>
            <th>Picture</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          {this.props.rentspaces.map(rentspace => (
            <Rentspace
              key={rentspace.id}
              rentspace={rentspace}
              onRentspaceChange={this.props.onRentspaceChange}
            />
          ))}
        </tbody>
      </Table>
    );
  }
}
