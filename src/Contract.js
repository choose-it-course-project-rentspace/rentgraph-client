import React, {Component} from "react";
import {Button} from "reactstrap";
import ContractModal from "./ContractModal";
import Auth from "./Auth.js";

export default class Contract extends Component {
  constructor(props) {
    super(props);

    this.deleteContract = this.deleteContract.bind(this);
  }

  deleteContract(id) {
    if (window.confirm("Are you sure you want to delete this Contract?")) {
      fetch("http://localhost:8080/contract/" + id, {
        method: "DELETE",
        headers: Auth.addAuthorizationHeader({})
      }).then(response => this.props.onContractChange());
    }
  }

  render() {
    return (
      <tr>
        <td>{this.props.contract.id}</td>
        <td>{this.props.contract.startDate}</td>
        <td>{this.props.contract.endDate}</td>
        <td>{this.props.contract.tenantId}</td>
        <td>{this.props.contract.rentalId}</td>
        <td>{this.props.contract.active}</td>
        <td>
          <div className="contractRowButtons">
            <Button
              className="btn btn-danger"
              onClick={() => this.deleteContract(this.props.contract.id)}
            >
              Delete
            </Button>
          </div>
          <ContractModal
            buttonLabel="Change"
            modalTitle="Change Contract"
            contract={this.props.contract}
            onContractChange={this.props.onContractChange}
            color="dark"
          />
        </td>
      </tr>
    );
  }
}
