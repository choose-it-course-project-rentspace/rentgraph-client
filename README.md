
Initial set-up:

1. Clone both rentgraph-api and rentgraph-client projects

2. Make sure you have Gradle (e.g. gradle-5.2.1) installed 
with appropriate PATH system environment variable in place

3. Make sure you have nodejs installed


4. Make sure you have PostgreSQL installed with an administration platform (e.g. pgAdmin 4)

during set-up/installation:
- set up password as "tere" 
or change the corresponding line in "./rentgraph-api/src/main/resources/application.properties" with your chosen password
- set port to 5432 
or change corresponding line in the application.properties file mentioned above

5. Open git bash or terminal of choice in path of rentgraph-client folder, run commands "npm install" and "npm start"

6. Open PostgreSQL admin platform.
In pgAdmin 4:
right click on "Databases", 
create new database named "rentgraph", 
right-click on it, choose "Restore..." from dropdown menu, 
click on "..." from right end of "Filename" bar and navigate to "Final_rentgraphSQL_BACKUP.tar", located in path:
"./rentgraph-api/src/main/resources/"
Make sure to have "All Files" selected from the "Format" drop-down menu.
Click "Select". Click "Restore".

7. Build and run rentgraph-api (in IntelliJ: from right sidemenu "Gradle").

8. Open "http://localhost:3000/" url using browser of choice, enter "a" as username and "a" as passwor. Click on "Log in".

In the current state there are still pretty significant bugs in element rendering, so would be a good idea to refresh the page when "all tables" are hidden.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
